#include "notify.h"

#include <Foundation/Foundation.h>
#include <objc/runtime.h>
#import <UserNotifications/UserNotifications.h>

#include <QDebug>
#include <QPixmap>
#include <QTimer>
#include <QtMac>

#define VERSION2 @available(macOS 10.14, *)
#define VERSION1 @available(macOS 10.10, *)
#define USE_UN_API

// Step 1: Enable feedback

@interface UserNSNotificationCenter : NSObject <NSUserNotificationCenterDelegate> {
}

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center
       shouldPresentNotification:(NSUserNotification *)notification;

@end

@implementation UserNSNotificationCenter

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center
       shouldPresentNotification:(NSUserNotification *)notification {
    Q_UNUSED(center)
    Q_UNUSED(notification)
    return YES;
}

- (void)userNotificationCenter:(NSUserNotificationCenter *)center
       didActivateNotification:(NSUserNotification *)notification {
    uint id = uint([notification.identifier intValue]);

    qDebug() << "action" << id << notification.activationType;

    if (
      notification.activationType ==
      NSUserNotificationActivationTypeContentsClicked) {
        Notify::instance->actionInvoked(id, "default");
    }
    else if (
      notification.activationType ==
      NSUserNotificationActivationTypeActionButtonClicked) {
        if (Notify::notifications.contains(id))
            Notify::instance->actionInvoked(
              id, Notify::notifications[id].actions[0].first);
    }

    // The notification are not deleted automatically on OS X
    [center removeDeliveredNotification:notification];
//    [notification release];
}
@end

#ifdef USE_UN_API

@interface UserUNNotificationCenter : NSObject <UNUserNotificationCenterDelegate> {
}
@end

@implementation UserUNNotificationCenter

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:
           (void (^)(UNNotificationPresentationOptions options))
             completionHandler NS_AVAILABLE_MAC(10.14) {
    Q_UNUSED(center);
    Q_UNUSED(notification);
    completionHandler(UNNotificationPresentationOptionAlert +
                      UNAuthorizationOptionSound);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
  didReceiveNotificationResponse:(UNNotificationResponse *)response
           withCompletionHandler:(void (^)(void))completionHandler NS_AVAILABLE_MAC(10.14) {
    NSString * idStr = response.notification.request.identifier;
    uint       id    = static_cast<uint>([idStr intValue]);

    if (response.actionIdentifier == UNNotificationDefaultActionIdentifier) {
        Notify::instance->actionInvoked(id, "default");
    }
    else if (
      response.actionIdentifier == UNNotificationDismissActionIdentifier) {
        Notify::instance->notificationClosed(id, 0);
    }
    else {
        Notify::instance->actionInvoked(id, QString::fromNSString(response.actionIdentifier));
    }

    [center removeDeliveredNotificationsWithIdentifiers:@[idStr]];
    completionHandler();
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
   openSettingsForNotification:(UNNotification *)notification NS_AVAILABLE_MAC(10.14) {

    NSString * idStr = notification.request.identifier;
    uint       id    = static_cast<uint>([idStr intValue]);

    Notify::instance->notificationClosed(id, 0);
    [center removeDeliveredNotificationsWithIdentifiers:@[idStr]];
    qDebug() << "Request to open setting, but not supported";
    return;
}

@end

#endif

// Step 2: Define Notify class

namespace backend {
static uint count = 0;
}

Notify::Notify(const QString & appName, QObject * parent)
    : QObject(parent)
    , appName(appName) {
    instance = this;

    #ifdef USE_UN_API
    if (VERSION2) {
        [[UNUserNotificationCenter currentNotificationCenter] setDelegate:[[UserUNNotificationCenter alloc] autorelease]];
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:
                 (UNAuthorizationOptionAlert +
                  UNAuthorizationOptionSound)
           completionHandler:^(BOOL granted, NSError * _Nullable error) {
              qDebug() << "request" << granted << error;
        }];
    }
    else
#endif
    if (VERSION1) {
        [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:[[UserNSNotificationCenter alloc] autorelease]];
    }
}

Notify::~Notify() {
    #ifdef USE_UN_API
    if (VERSION2) {
        [[UNUserNotificationCenter currentNotificationCenter] setDelegate:nil];
    }
    else
#endif
    if (VERSION1) {
        [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:nil];
    }
};

uint Notify::pushNotification(const Notification & notif) {
    uint id = ++backend::count;

    NSUserNotification * userNotif = [[NSUserNotification alloc] init];
    NSString *           notifId   = QString::number(id).toNSString();

    #ifdef USE_UN_API
    if (VERSION2) {
        UNMutableNotificationContent * content = [UNMutableNotificationContent alloc];

        content.title              = notif.header.toNSString();
        content.subtitle           = notif.subheader.toNSString();
        content.body               = notif.text.toNSString();
        content.categoryIdentifier = appName.toNSString();
        content.threadIdentifier   = notif.typeId.toNSString();

        if (notif.silent)
            content.sound =
              [UNNotificationSound defaultCriticalSoundWithAudioVolume:0.0];

        UNNotificationRequest * userNotif =
          [UNNotificationRequest requestWithIdentifier:notifId content:content trigger:nil];

        [[UNUserNotificationCenter currentNotificationCenter]
          addNotificationRequest:userNotif withCompletionHandler:^(NSError * error) {
            qDebug() << "notify creation error" << error;
          }];
    }
    else
#endif
    if (VERSION1) {
        userNotif.identifier      = notifId;
        userNotif.title           = notif.header.toNSString();
        userNotif.subtitle        = notif.subheader.toNSString();
        userNotif.informativeText = notif.text.toNSString();

//        if (!notif.contentIconImage.isNull())
//            userNotif.contentImage =
//              QtMac::toNSImage(QPixmap::fromImage(notif.contentIconImage));

        if (!notif.actions.isEmpty()) {
            userNotif.hasActionButton = YES;
            userNotif.actionButtonTitle =
              notif.actions.first().second.toNSString();

            if (notif.actions.begin() != notif.actions.end()) {
                userNotif.otherButtonTitle =
                  notif.actions.last().second.toNSString();
            }
        }

        userNotif.hasReplyButton = NO;

        notifications[id]    = notif;
        notifications[id].ns = userNotif;

        [[NSUserNotificationCenter defaultUserNotificationCenter]
          deliverNotification:userNotif];
    }

    QTimer::singleShot(300000, [this, id]() { this->hideNotification(id); });

    qDebug() << "sended" << id;
    return id;
}

uint Notify::pushAttach(const Notification & notif) {
    Q_UNUSED(notif)
    return 0;
}

void Notify::hideNotification(uint id) {
    if (!notifications.contains(id))
        return;

#ifdef USE_UN_API
    if (VERSION2) {
        NSString * notifId = QString::number(id).toNSString();

        [[UNUserNotificationCenter currentNotificationCenter]
          removeDeliveredNotificationsWithIdentifiers:@[notifId]];
    }
    else
#endif
    if (VERSION1) {
        auto userNotif = notifications[id].ns;

        [[NSUserNotificationCenter defaultUserNotificationCenter]
          removeDeliveredNotification:userNotif];
    }

    notifications.remove(id);
}

void Notify::clear() {

    #ifdef USE_UN_API
    if (VERSION2) {
        [[UNUserNotificationCenter currentNotificationCenter]
          removeAllDeliveredNotifications];
    }
    else
#endif
    if (VERSION1) {
        [[NSUserNotificationCenter defaultUserNotificationCenter]
          removeAllDeliveredNotifications];
    }

    notifications.clear();
}

void Notify::actionInvoked(uint id, const QString & action) {
    if (!notifications.contains(id))
        return;

    if (action == "default") {
        emit clicked(notifications[id]);
    }
    else {
        emit triggered(notifications[id], action);
    }

    notifications.remove(id);
}
void Notify::notificationClosed(uint id, uint /*reason*/) {
    if (!notifications.contains(id))
        return;

    emit closed(notifications[id]);
    notifications.remove(id);
}
