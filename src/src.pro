TARGET = -icl-notify
TEMPLATE = lib

QT += core gui network

DEFINES += LIBNOTIFY_LIBRARY

unix:!mac {
    QT += dbus
}

mac {
    QT += macextras
}

CONFIG += c++17 no_include_pwd
CONFIG -= app_bundle

!win32 {
    QMAKE_CXXFLAGS += -std=c++17
    DESTDIR = $$PWD/../bin/lib
}
else {
    QMAKE_CXXFLAGS += /std:c++17
    QMAKE_LFLAGS += /MACHINE:X64
    DESTDIR = $$PWD/../bin
}

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

HEADERS += notify.h \
    global.h

mac {
    LIBS += -framework Foundation -framework Cocoa
    LIBS += -framework UserNotifications
    SOURCES += notifymac.mm
}
else:win32 {
    HEADERS += wintoastlib.h
    SOURCES += notifywin.cpp \
               wintoastlib.cpp
}
else {
    SOURCES += notifylinux.cpp
}

unix {
    target.path = /usr/lib
    INSTALLS += target
}

SOURCES += \
    notify.cpp

